﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    public enum SpawnState { SPAWNING, WAITING, COUNTING}

    [System.Serializable]
	public class Wave
    {
        public string name;
        public Transform enemy;
        public Transform animalBiche;
        public Transform animalLapin;
        public int countEnemy;
        public float rateEnemy;
        public int countLapin;
        public float rateLapin;
        public int countBiche;
        public float rateBiche;
    }

    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 5f;
    public float waveCountDown;

    private float searchCountDown = 1f; 

    private SpawnState state = SpawnState.COUNTING;

    private void Start()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        waveCountDown = timeBetweenWaves;
    }

    private void Update()
    {
        if(state == SpawnState.WAITING)
        {
            if (!EnemyIsALive())
            {
                WaveCompleted();
            }
            else
            {
                return;
            }
        }

        if (waveCountDown <= 0)
        {
            if(state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    void WaveCompleted()
    {
        Debug.Log("Wave Completed");

        state = SpawnState.COUNTING;
        waveCountDown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            Debug.Log("COMPLETED! Looping...");
        }

        nextWave++;
    }

    bool EnemyIsALive()
    {
        searchCountDown -= Time.deltaTime;

        if(searchCountDown <= 0f)
        {
            searchCountDown = 1f;
            if(GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }

        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        Debug.Log("Spawning Wave : " + _wave.name);
         
        state = SpawnState.SPAWNING;

        for (int i = 0; i < _wave.countEnemy; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.rateEnemy);
        }

        for (int i = 0; i < _wave.countBiche; i++)
        {
            SpawnAnimal(_wave.animalBiche);
            yield return new WaitForSeconds(1f / _wave.rateBiche);
        }

        for (int i = 0; i < _wave.countLapin; i++)
        {
            SpawnLapin(_wave.animalLapin);
            yield return new WaitForSeconds(1f / _wave.rateLapin);
        }

        state = SpawnState.WAITING;

        yield break;
    }

    void SpawnEnemy(Transform _enemy)
    {
        Debug.Log("Spawning enemy : " + _enemy.name);

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, _sp.position, _sp.rotation);
    }

    void SpawnAnimal(Transform _animal)
    {
        Debug.Log("Spawning animal : " + _animal.name);

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_animal, _sp.position, _sp.rotation);
    }

    void SpawnLapin(Transform _animal)
    {
        Debug.Log("Spawning animal : " + _animal.name);

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_animal, _sp.position, _sp.rotation);
    }
}
