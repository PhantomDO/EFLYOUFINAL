﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Wave[] waves;
    public EnemyController enemy;
    public AnimalController animal;

    Wave currentWave;
    int currentWaveNumber;

    int enemiesRemainingToSpawn;
    int enemiesRemainingAlive;

    int animalsRemainingToSpawn;
    int animalsRemainingAlive;

    float nextSpawnTime;

    bool allAnimalsDied, allEnemiesDied;

    private void Start()
    {
        NextWave();
    }

    private void Update()
    {
        if (enemiesRemainingToSpawn > 0 && Time.time > nextSpawnTime)
        {
            enemiesRemainingToSpawn--;
            nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

            EnemyController spawnedEnemy = Instantiate(enemy, transform.position, Quaternion.identity) as EnemyController;
            spawnedEnemy.OnDeath += OnEnemyDeath;
        }

        if (animalsRemainingToSpawn > 0 && Time.time > nextSpawnTime)
        {
            animalsRemainingToSpawn--;
            nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

            AnimalController spawnedAnimal = Instantiate(animal, transform.position, Quaternion.identity) as AnimalController;
            spawnedAnimal.OnDeath += OnAnimalDeath;
        }
    }

    void OnEnemyDeath()
    {
        enemiesRemainingAlive--;

        if (enemiesRemainingAlive == 0)
        {
            NextWave();
            Debug.Log("C");
        }
    }

    void OnAnimalDeath()
    {
        Debug.Log("O");
        animalsRemainingAlive--;

        if (animalsRemainingAlive == 0)
        {
            NextWave();
            Debug.Log("B");
        }
    }

    void NextWave()
    {
        allEnemiesDied = false;
        allAnimalsDied = false;

        currentWaveNumber++;

        print("Wave : " + currentWaveNumber);

        if (currentWaveNumber - 1 < waves.Length)
        {
            currentWave = waves[currentWaveNumber - 1];
        }

        enemiesRemainingToSpawn = currentWave.enemyCount;
        enemiesRemainingAlive = enemiesRemainingToSpawn;

        animalsRemainingToSpawn = currentWave.animalCount;
        animalsRemainingAlive = animalsRemainingToSpawn;
    }

    [System.Serializable]
	public class Wave
    {
        public int enemyCount;
        public int animalCount; 
        public float timeBetweenSpawns;
    }
}
