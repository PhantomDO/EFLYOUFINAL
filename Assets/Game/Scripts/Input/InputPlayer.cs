﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Input", menuName =("Input"))]
public class InputPlayer : ScriptableObject
{
    public string controllerX;
    public string controllerZ;
    public string controllerA;
}
