﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthAndDie : MonoBehaviour {

    public float currentHealth;
    private float minHealth;
    public float maxHealth;

    bool dead;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void Update()
    {
        if (currentHealth <= minHealth)
        {
            dead = true;
            if (dead == true)
            {
                Debug.Log("You Die");
                Respawn();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Water")
        {
            Debug.Log("Dead");
            Respawn();
        }
    }

    private void Respawn()
    {
        transform.parent.GetComponent<PowerTransmit>().powerDown = true;
        transform.position -= new Vector3(0, -10, 5);
        currentHealth = maxHealth;
        dead = false;
    }
}
