﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PowerTransmit : MonoBehaviour {

    [HideInInspector]
    public bool powerUp;
    [HideInInspector]
    public bool powerDown;

    [Header("Leveling")]
    private int level = 1;
    public int currentLevel;
    public string battleScene;

    [Header("LR At")]
    public Transform[] points;
    private LineRenderer lr;

    private void Start()
    {
        lr = GetComponent<LineRenderer>();
        lr.positionCount = points.Length;
    }

    private void Update()
    {
        currentLevel = level;

        if (currentLevel < 1)
        {
            SceneManager.LoadScene(battleScene);
        }

        for (int i = 0; i < level; i++)
        {
            UpdateLeveling();
        }
    }

    private void LateUpdate()
    {
        Link();
    }

    public void UpdateLeveling()
    {
        if (powerUp)
        {
            level++;
            for (int i = 0; i < transform.GetComponentsInChildren<PlayerController>().Length; i++)
            {
                transform.GetComponentsInChildren<PlayerController>()[i].IncreaseCapacity();
                powerUp = false;
            }
        }

        if (powerDown)
        {
            level--;
            for (int i = 0; i < transform.GetComponentsInChildren<PlayerController>().Length; i++)
            {
                transform.GetComponentsInChildren<PlayerController>()[i].DecreaseCapacity();
                powerDown = false;
            }
        }
    }

    void Link()
    {
        for (int i = 0; i < points.Length; ++i)
        {
            lr.SetPosition(i, points[i].position);
        }
    }



}
