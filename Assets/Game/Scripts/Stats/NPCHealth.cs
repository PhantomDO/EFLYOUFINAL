﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCHealth : MonoBehaviour, IDamageable  {

    public float currentHealth;
    public float maxHealth;

    private bool dead;

    public event System.Action OnDeath;

    protected virtual void Start ()
    {
		currentHealth = maxHealth;
	}

    public void TakeHit(float damage)
    {
        currentHealth -= damage;

        if (currentHealth <= 0 && !dead)
        {
            Die();
        }
    }

    protected void Die()
    {
        dead = true;
        if (OnDeath != null)
        {
            OnDeath();
        }
        GameObject.Destroy(gameObject);
    }
}
