﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : NPCHealth {

    [Header("Searching")]
    public LayerMask searchForPlayer;
    public float radius;
    public float viewAngle;
    public List<GameObject> visibleTarget = new List<GameObject>();
    private GameObject target;
    public float damage;

    [Header("References")]
    private LineRenderer lr;
    private Light spot;
    private NavMeshAgent agent;
    private string state = "idle";

    [Header("Animation")]
    public Animator AnimationHurt;


    protected override void Start () {
        agent = GetComponent<NavMeshAgent>();
        spot = GetComponentInChildren<Light>();
        lr = GetComponentInChildren<LineRenderer>();
        spot.spotAngle = viewAngle;
	}
	
	void Update () {
        visibleTarget.Clear();
        lr.enabled = false;
        Collider[] fov = Physics.OverlapSphere(transform.position, radius, searchForPlayer);

        if (state == "idle")
        {
            Vector3 randomPos = Random.insideUnitSphere * 20f;
            NavMeshHit navHit;
            NavMesh.SamplePosition(transform.position + randomPos, out navHit, 20f, NavMesh.AllAreas);
            agent.SetDestination(navHit.position);
            state = "walk";
        }
        if (state == "walk")
        {
            if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
            {
                state = "idle";
            }
        }

        for (int i = 0; i < fov.Length; i++)
        {
            target = fov[i].gameObject;
            Vector3 direction = (target.transform.position - transform.position);

            if (Vector3.Angle(transform.forward, direction) < viewAngle / 2)
            {
                float distance = Vector3.Distance(transform.position, target.transform.position);
                agent.SetDestination(target.transform.position);

                if(Physics.Raycast(transform.position, direction, distance, searchForPlayer))
                {
                    visibleTarget.Add(target);
                    Debug.DrawRay(transform.position, direction);
                    //LINE RENDERER
                    lr.enabled = true;
                    lr.SetPosition(0, transform.position);
                    lr.SetPosition(1, target.transform.position);
                    ApplyDamage();
                    AnimationHurt.SetBool("HurtBool", false);
                }
            }
        }
	}

    private void ApplyDamage()
    {
        target.GetComponentInChildren<HealthAndDie>().currentHealth -= (damage) * Time.deltaTime;
        AnimationHurt.SetBool("HurtBool", true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
