﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject target;
    public Camera cam;

    public float distance;
    public float speedMove;

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.transform.position, speedMove * Time.deltaTime);
    }
}
