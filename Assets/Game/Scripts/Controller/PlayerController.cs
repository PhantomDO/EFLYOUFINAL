﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour {

    [Header("Input")]
    public InputPlayer input;
    private float controllerX;
    private float controllerZ;
    private float controllerA;

    [Header("Movement")]
    public float startTime;
    public float moveSpeed;
    public float gravity = -12;
    private float velocityY;

    [Header("Attribute")]
    public float damage;
    private float currentDamage;
    public float distanceDash;
    private float currentDistanceDash;
    public float dashSpeed;

    [Header("Modificators")]
    public float increaseDamage = 2;

    [Header("Smooth")]
    public float smoothTime = 0.2f;
    private float turnSmoothVelocity;
    private Vector3 move;
    private Vector3 velocity;

    [Header("References")]
    public CharacterController controller;
    private Rigidbody rB;
    public Transform targetDash;
    public LayerMask everyCollisions;
    public Transform checkCollide;
    public SphereCollider sphereCol;

    [Header("Attack")]
    float distance;
    public LayerMask catchEnemy;
    [Range(0,1)]
    public float radius;
    [Range(0, 1)]
    public float fireRate;
    private float nextFire;

    [Header("Animation")]
    public Animator AnimationDarkElf;

    void Start ()
    {
        rB = GetComponent<Rigidbody>();
        radius = sphereCol.radius;
        startTime = Time.time;
    }
	
	void Update ()
    {
        Move();
        Attack();
        Capacity();
        //CheckCollisions();
    }

    private void Move()
    {
        controllerX = Input.GetAxisRaw(input.controllerX);
        controllerZ = Input.GetAxisRaw(input.controllerZ);
        controllerA = Input.GetAxisRaw(input.controllerA);

        move = new Vector3(controllerX, 0, controllerZ);
        Vector3 moveDir = move.normalized;

        AnimationDarkElf.SetBool("WalkBool", false);

        if (moveDir != Vector3.zero)
        {
            float targetRot = Mathf.Atan2(move.x, move.z) * Mathf.Rad2Deg;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRot, ref turnSmoothVelocity, smoothTime);

            AnimationDarkElf.SetBool("WalkBool", true);
        }

        velocityY += Time.deltaTime * gravity;
        velocity = moveDir * moveSpeed + Vector3.up * velocityY;
        controller.Move(velocity * Time.deltaTime);

        velocity = Vector3.zero;

        if (controller.isGrounded)
        {
            velocityY = 0;
        }
    }

    private void Attack()
    {
        AnimationDarkElf.SetBool("AttackBool", false);

        if (controllerA != 0 && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            rB.transform.position = Vector3.Lerp(transform.position, targetDash.position, dashSpeed * Time.deltaTime);

            Collider[] col = Physics.OverlapBox(transform.position, transform.localScale, transform.rotation, catchEnemy);

            AnimationDarkElf.SetBool("AttackBool", true);

            for (int i = 0; i < col.Length; i++)
            {
                Debug.Log("col : " + col[i].name);
                IDamageable damageableNPC = col[i].GetComponent<Collider>().GetComponent<IDamageable>();
                if (damageableNPC != null)
                {
                    damageableNPC.TakeHit(currentDamage);
                }
                transform.parent.GetComponent<PowerTransmit>().powerUp = true;
            }
        }
    }

    private void CheckCollisions()
    {      
    }

    private void Capacity()
    {
        distance = Vector3.Distance(transform.position, targetDash.position);

        distanceDash = Mathf.Clamp(distanceDash, 3, 10);
        radius = Mathf.Clamp(radius,1, 3);
        transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x, 1, 5), Mathf.Clamp(transform.localScale.y, 1, 5), Mathf.Clamp(transform.localScale.z, 1, 5));
        // Damage
        currentDamage = damage;
        // Distance Dash
        currentDistanceDash = distanceDash;
        targetDash.localPosition = new Vector3(0, 0, currentDistanceDash);
    }

    public void IncreaseCapacity()
    {
        damage += increaseDamage;
        transform.localScale *= 1.5f;
    }

    public void DecreaseCapacity()
    {
        damage -= increaseDamage;
        transform.localScale /= 1.5f;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, transform.localScale*2);
    }
}
