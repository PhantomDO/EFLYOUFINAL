﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AnimalController : NPCHealth {

    [Header("Searching")]
    public LayerMask fearForPlayer;
    public float radius;
    public float viewAngle;
    public List<GameObject> visiblePlayer = new List<GameObject>();
    private GameObject player;

    [Header("References")]
    private NavMeshAgent agent;
    private string state = "idle";

    protected override void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
    }
	
	void Update ()
    {
        visiblePlayer.Clear();
        Collider[] fov = Physics.OverlapSphere(transform.position, radius, fearForPlayer);

        if(state == "idle")
        {
            Vector3 randomPos = Random.insideUnitSphere * 20f;
            NavMeshHit navHit;
            NavMesh.SamplePosition(transform.position + randomPos, out navHit, 20f, NavMesh.AllAreas);
            agent.SetDestination(navHit.position);
            state = "walk";
        }
        if(state == "walk")
        {
            if (agent.remainingDistance <= agent.stoppingDistance && !agent.pathPending)
            {
                state = "idle";
            }
        }

        for (int i = 0; i < fov.Length; i++)
        {
            player = fov[i].gameObject;
            Vector3 direction = (transform.position - player.transform.position);

            if (Vector3.Angle(transform.forward, direction) < viewAngle / 2)
            {
                float distance = Vector3.Distance(transform.position, player.transform.position);
                NavMeshHit navHit;
                NavMesh.SamplePosition(transform.position + direction, out navHit, Mathf.Infinity, NavMesh.AllAreas);
                agent.SetDestination(navHit.position);

                if (Physics.Raycast(transform.position, direction, distance, fearForPlayer))
                {
                    visiblePlayer.Add(player);
                    agent.speed *= 2;
                    agent.acceleration *= 2;
                    Debug.DrawRay(transform.position, direction, Color.red);
                }
            }
        }
	}


    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
