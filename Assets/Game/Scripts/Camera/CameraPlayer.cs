﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CamRect", menuName = ("CamRect"))]
public class CameraPlayer : ScriptableObject {

    public float x, y, w, h;
}
