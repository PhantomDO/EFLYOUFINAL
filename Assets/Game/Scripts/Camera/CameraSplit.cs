﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSplit : MonoBehaviour {

    private Camera cam;
    public CameraPlayer camPlayer;

    void Start()
    {
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        cam.rect = new Rect(camPlayer.x, camPlayer.y, camPlayer.w, camPlayer.h);
    }
}
